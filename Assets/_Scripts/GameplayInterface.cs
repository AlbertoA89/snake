﻿using UnityEngine;

public class GameplayInterface : MonoBehaviour {

	[SerializeField] private GameObject _snake;
	[SerializeField] private GameObject _gameoverCanvasPanel;

	private void Update()
	{
		_gameoverCanvasPanel.SetActive(SnakeBehaviour.Gameover);
	}

	public void Play()
	{
		SnakeBehaviour.Gameover = false;

		ResetGame();
	}

	private void ResetGame()
	{
		_snake.transform.position = Vector3.zero;
		SnakeBehaviour.ResetTail();
		InputSystem.CurrentDirection = Vector3.right;
		FoodSpawner.ResetFood();
	}
}
