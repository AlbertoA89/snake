﻿using UnityEngine;
using UnityEngine.Serialization;

public class FoodSpawner : MonoBehaviour {
	
	#region Variables
	
	[FormerlySerializedAs("Food")] public GameObject FoodPrefab;
	public Transform TopWall;
	public Transform RightWall;
	public Transform BottomWall;
	public Transform LeftWall;

	public static GameObject _currentFood;
	
	private int margin = 1;
	
	#endregion
	
	private void Start ()
	{
		_currentFood = SpawnFood();
	}

	private void Update()
	{
		if(_currentFood != null) return;
		
		_currentFood = SpawnFood();
	}

	private GameObject SpawnFood()
	{
		//food random position between walls
		int x = (int)Random.Range(LeftWall.position.x + margin, RightWall.position.x - margin);
		int y = (int)Random.Range(BottomWall.position.y + margin, TopWall.position.y - margin);
		Vector3 foodPosition = new Vector3(x, y, 0);
		
		//check snake and tail position
		Collider2D[] hitColliders = Physics2D.OverlapCircleAll(foodPosition, 3);
		foreach (var hit in hitColliders)
		{
			//TODO: solve without recursive function
			if (hit.transform.CompareTag("Snake"))
				return SpawnFood();	
		}

		return Instantiate(FoodPrefab, foodPosition, Quaternion.identity);
	}

	public static void ResetFood()
	{
		Destroy(_currentFood);
		_currentFood = null;
	}
}
