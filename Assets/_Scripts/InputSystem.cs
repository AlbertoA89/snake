﻿using UnityEngine;

public class InputSystem : MonoBehaviour {

	public static Vector3 CurrentDirection = Vector3.right;
	
	public static Vector3 GetInputs()
	{
		if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
		{
			CurrentDirection = Vector3.up;
		}
		if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
		{
			CurrentDirection = Vector3.down;
		}
		if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
		{
			CurrentDirection = Vector3.left;
		}
		if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
		{
			CurrentDirection = Vector3.right;
		}
		return CurrentDirection;
	}
}
