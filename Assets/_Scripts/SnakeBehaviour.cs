﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(Rigidbody2D))]
public class SnakeBehaviour : MonoBehaviour {

	#region Variables
	
	[HideInInspector] public static List<Transform> _tail = new List<Transform>();
	[HideInInspector] public static bool Gameover;

	[Header("Objects")]
	[SerializeField] private GameObject _tailPrefab;
	
	[Header("Tune")]
	[Range(0, 10)] [SerializeField] private float _speed = .4F;
	[Range(0, 100)] [SerializeField] private float _waitForWalk = 20;
	private float _waited;
	private float _initialStepSpeed;
	
	private Vector3 _currentDirection = Vector3.right;
	private Vector3 _currentPosition;
	
	#endregion
	
	#region MonoBehaviour Methods

	private void Start()
	{
		_initialStepSpeed = _waitForWalk;
	}

	private void Update()
	{
		if(Gameover) return;
		
		Move();
	}

	private void FixedUpdate ()
	{
		_currentDirection = InputSystem.GetInputs();
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Snake") || other.CompareTag("LimitWall"))
		{
			Gameover = true;
			ResetSnake();
			return;
		}

		if (other.CompareTag("Food"))
		{
			Ate(other.gameObject);
		}
	}

	#endregion
	
	#region Actions Methods
	
	private void Move()
	{
		_waited++;
		if (_waited < _waitForWalk) return;
		
		MoveTail();
		_currentPosition += _currentDirection;
		transform.position = _currentPosition;
		_waited = 0;
	}

	private void MoveTail()
	{
		if (_tail.Count <= 0) return;
		
		for (int i = _tail.Count-1; i > 0; i--)
		{
			if (i > 0)
				_tail[i].transform.position = _tail[i - 1].transform.position;
		}
		_tail[0].transform.position = transform.position;
		_tail.Last().GetComponent<Collider2D>().enabled = true;
	} 

	private void Ate(GameObject fruit)
	{
		Transform g = Instantiate(_tailPrefab, transform.position, Quaternion.identity).transform;
		_tail.Add(g);
		
		Destroy(fruit);

		IncreaseSnakeSpeed();
	}

	private void IncreaseSnakeSpeed()
	{
		//TODO: better non-linear progression
		_waitForWalk -= _speed;
		if(_speed >= .3F)
			_speed /= 1.25F;
	}

	#endregion
	
	#region Reset Methods

	public void ResetSnake()
	{
		_currentPosition = Vector3.zero;
		_currentDirection = Vector3.right;
		_waitForWalk = _initialStepSpeed;
	}

	public static void ResetTail()
	{
		foreach (var tail in _tail)
		{
			Destroy(tail.gameObject);	
		}
		_tail.Clear();
	}

	#endregion
}
